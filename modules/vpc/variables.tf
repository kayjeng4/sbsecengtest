variable "vpcsubnetname" { 
    type = string
 }
variable "vpcname" { 
    type = string
 }
locals {
  network_settings = {
    "vpc1"  = { location = "us-central1", auto_create_subnetworks = false, subnet = "mysubnet1", cidr = "192.168.1.0/24" },
    "vpc2"   = { location = "us-central1", auto_create_subnetworks = false, subnet = "mysubnet2", cidr = "192.168.2.0/24" },
    #"vpc3" = { location = "us-central1", auto_create_subnetworks = true }
  }
}
