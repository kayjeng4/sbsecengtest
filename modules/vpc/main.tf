
resource "google_compute_subnetwork" "vpn_subnets" {
  for_each = local.network_settings
  name          = "${ each.value.subnet}"
  ip_cidr_range = "${ each.value.cidr}"
  region        = "${ each.value.location}"
  network       = google_compute_network.vpc_network[each.key].id
}

resource "google_compute_network" "vpc_network" {
  for_each = local.network_settings
  name = "${ each.key }"
  auto_create_subnetworks = each.value.auto_create_subnetworks
}

resource "google_compute_network_peering" "peering1" {
  name         = "peering1"
  #for_each     = local.network_settings
  network      = google_compute_network.vpc_network["vpc1"].self_link
  peer_network = google_compute_network.vpc_network["vpc2"].self_link
}

resource "google_compute_network_peering" "peering2" {
  name         = "peering2"
  #for_each     = local.network_settings
  network      = google_compute_network.vpc_network["vpc2"].self_link
  peer_network = google_compute_network.vpc_network["vpc1"].self_link
}
