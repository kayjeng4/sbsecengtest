#! /bin/bash
echo "Hello World!  Check back for IdM test results after AMT completes in a few minutes."

yum update -y
yum install git -y
yum install wget -y
yum install ansible -y
#yum install docker -y
#yum install python3, python3-pip -y
#pip3 install --upgrade pip
#pip3 install requests
#pip3 install pyOpenSSL

wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
yum upgrade -y
# Add required dependencies for the jenkins package
yum install java-11-openjdk -y
yum install jenkins -y
systemctl daemon-reload
systemctl enable jenkins
systemctl start jenkins

YOURPORT=8080
PERM="--permanent"
SERV="$PERM --service=jenkins"

firewall-cmd $PERM --new-service=jenkins
firewall-cmd $SERV --set-short="Jenkins ports"
firewall-cmd $SERV --set-description="Jenkins port exceptions"
firewall-cmd $SERV --add-port=$YOURPORT/tcp
firewall-cmd $PERM --add-service=jenkins
firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --reload

echo "Startup script Completed!"