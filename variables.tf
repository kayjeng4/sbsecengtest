variable "vpc_name" {
    type = string 
    default = "vpcautotest1"
}
variable "project_id" {
    type = string 
    default = "sab-kjgcp-april22-sbx-8397"
}
variable "vpcsubnet_name" {
    type = string 
    default = "vpcsubnetautotest1"
}